from setuptools import setup


setup(
    name='asyncorm',
    version='0.1a1',
    packages=['asyncorm', 'asyncorm.models', 'asyncorm.models.fields',
              'asyncorm.backends', 'asyncorm.backends.sqlite3',
              'asyncorm.backends.postgresql', 'asyncorm.interfaces',
              'asyncorm.expressions', 'asyncorm.utils'],
    url='https://bitbucket.org/Vetal_krot/asyncorm',
    license='GPLv2',
    author='Vetal_krot',
    author_email='VetaLkrot@gmail.com',
    description='Non blocking ORM for asyncio',
    install_requires=[
        'pytz',
        'Shapely',
    ]
)
