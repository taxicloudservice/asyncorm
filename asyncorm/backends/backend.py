import asyncio

from asyncorm.log import logger
from asyncorm.interfaces.backend import IBackend
from asyncorm.backends.transaction import SqlTransaction


class BackendFeatures(object):
    support_returning = False
    support_for_update = False

    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)


@IBackend.register
class BackendBase(object):
    connection_pool_factory = NotImplemented
    transaction_factory = SqlTransaction
    features = BackendFeatures()
    compiler = None

    def __init__(self, database, user, password, host, *, loop=None,
                 port=None, **kwargs):
        self.database = database
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self._lock = asyncio.Lock()
        self._loop = loop
        self._conn_kwargs = kwargs
        self._pool = None

    @property
    def pool(self):
        if not self._pool:
            raise RuntimeError('Backend {} is down'.format(self.__class__.__name__))
        return self._pool

    def get_connection(self):
        return self.pool.acquire()

    def create_connection_pool(self):
        return self.connection_pool_factory(
            dbname=self.database, user=self.user, password=self.password,
            host=self.host, port=self.port, **self._conn_kwargs)

    def get_features(self):
        return self.features

    def get_compiler(self):
        return self.compiler

    async def _run_trans(self, executor_name, *args, **kwargs):
        async with self.get_connection() as conn:
            trans = self.transaction_factory(conn, self.get_compiler(),
                                             self.get_features)
            executor = getattr(trans, executor_name)
            return await executor(*args, **kwargs)

    def db_insert(self, sql, params, fetch_result=True):
        return self._run_trans('db_insert', sql, params, fetch_result)

    def db_select(self, sql, params, fetch_result=True):
        return self._run_trans('db_select', sql, params, fetch_result)

    def db_update(self, sql, params, fetch_result=False):
        return self._run_trans('db_update', sql, params, fetch_result)

    def db_delete(self, sql, params, fetch_result=False):
        return self._run_trans('db_delete', sql, params, fetch_result)

    def db_transaction(self, func, *args, **kwargs):
        return self._run_trans('db_transaction', func, *args, **kwargs)

    async def start(self):
        logger.info('Starting backend %s', self.__class__.__name__)
        async with self._lock:
            if not self._pool:
                self._pool = await self.create_connection_pool()

    async def stop(self):
        logger.info('Stopping backend %s', self.__class__.__name__)
        async with self._lock:
            if self._pool:
                self._pool.close()
                await self._pool.wait_closed()
                self._pool = None
