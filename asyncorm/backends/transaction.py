from asyncorm.interfaces.transaction import ITransaction
from asyncorm.log import logger


@ITransaction.register
class SqlTransaction(object):

    def __init__(self, conn, compiler, features):
        self._conn = conn
        self.compiler = compiler
        self.features = features

    def get_features(self):
        return self.features

    def get_compiler(self):
        return self.compiler

    def cursor(self):
        return self._conn.cursor()

    async def _execute_sql(self, sql, params, fetch_result):
        async with self.cursor() as cursor:
            try:
                await cursor.execute(sql, params)
            except:
                logger.exception('SQL error: %s', cursor.query.decode('utf-8'))
            else:
                logger.debug('Executing SQL: %s', cursor.query.decode('utf-8'))
            if fetch_result:
                return await cursor.fetchall()

    async def db_insert(self, sql, params, fetch_result=True):
        async with self.cursor() as cursor:
            await cursor.execute(sql, params)
            logger.debug('Executing SQL: %s', cursor.query.decode('utf-8'))
            return [(cursor.lastrowid,)]

    def db_select(self, sql, params, fetch_result=True):
        return self._execute_sql(sql, params, fetch_result)

    def db_update(self, sql, params, fetch_result=False):
        return self._execute_sql(sql, params, fetch_result)

    def db_delete(self, sql, params, fetch_result=False):
        return self._execute_sql(sql, params, fetch_result)

    async def db_transaction(self, func, *args, **kwargs):
        return await func(self, *args, **kwargs)