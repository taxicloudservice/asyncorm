from asyncorm.backends.transaction import SqlTransaction


class PGTransaction(SqlTransaction):

    def db_insert(self, sql, params, fetch_result=True):
        return self._execute_sql(sql, params, fetch_result)