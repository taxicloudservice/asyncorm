from asyncorm.backends.compiler import (SQLCompiler, BaseSelectCompiler,
    BaseInsertCompiler, BaseUpdateCompiler,  BaseDeleteCompiler)


class ReturningMixin(object):

    def compile_returning(self):
        returning = self.container.get_returning()
        if returning:
            sql, params = self.compile_expression(returning)
            return 'RETURNING ' + sql, params
        return None, None


class SelectCompiler(BaseSelectCompiler):
    strategy = BaseSelectCompiler.strategy + ('for_update',)

    def compile_for_update(self):
        for_update, nowait = self.container.get_for_update()
        if for_update:
            return 'FOR UPDATE NOWAIT' if nowait else 'FOR UPDATE', None
        return None, None


class InsertCompiler(BaseInsertCompiler, ReturningMixin):
    strategy = BaseInsertCompiler.strategy + ('returning',)


class UpdateCompiler(BaseUpdateCompiler, ReturningMixin):
    strategy = BaseUpdateCompiler.strategy + ('returning',)


class DeleteCompiler(BaseDeleteCompiler, ReturningMixin):
    strategy = BaseDeleteCompiler.strategy + ('returning',)


class PGCompiler(SQLCompiler):
    insert_compiler = InsertCompiler
    select_compiler = SelectCompiler
    update_compiler = UpdateCompiler
    delete_compiler = DeleteCompiler
