from asyncorm.constants import EXPR_DELIMITER
from asyncorm.interfaces.field import IField
from asyncorm.interfaces.query import IQueryBuilder
from asyncorm.interfaces.compiler import ICompilable


def expression_from_string(model, expr_str, value):
    expr_name = 'equals'
    names = expr_str.split(EXPR_DELIMITER, 1)
    field_name = names.pop(0)
    field = getattr(model, field_name)
    if not field:
        raise AttributeError("Model {0} not have field {1}".format(
            model, field_name))
    if names:
        expr_name = names[0]
    return getattr(field, expr_name)(value)


def compile_value(value, compiler):
    sql, params = None, None
    if isinstance(value, IField):
        sql = value.get_sqlname()
    elif isinstance(value, ICompilable):
        sql, params = value.compile(compiler)
        if isinstance(value, IQueryBuilder):
            sql = '({0})'.format(sql)
    else:
        sql, params = compiler.formatter, [value]
    return sql, params


def compile_iterable(values, compiler):
    sql, params = [], []
    for value in values:
        _sql, _params = compile_value(value, compiler)
        if _sql:
            sql.append(_sql)
        if _params:
            params.extend(_params)
    return sql, params