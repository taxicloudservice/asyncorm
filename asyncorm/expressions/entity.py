from asyncorm.interfaces.model import IModel
from asyncorm.interfaces.field import IField
from asyncorm.interfaces.compiler import ICompilable
from asyncorm.expressions.operator import Node
from asyncorm.expressions.util import compile_value


__all__ = ['Table', 'Procedure', 'E']


@ICompilable.register
class Table(object):
    def __init__(self, table):
        self.table = table

    def compile(self, compiler):
        if isinstance(self.table, ICompilable):
            return compile_value(self.table, compiler)
        if isinstance(self.table, str):
            table_name = self.table
        elif (isinstance(self.table, IModel) or
                  issubclass(self.table, IModel)):
            table_name = self.table.get_meta().get_tablename()
        else:
            raise ValueError('Wrong table value')
        return '"{0}"'.format(table_name), None


@ICompilable.register
class Procedure(object):
    procedure_name = ''

    def __init__(self, *params):
        self.params = params

    def get_params(self):
        return self.params

    def compile(self, compiler):
        params = self.get_params()
        ap = ', '.join([compiler.formatter] * len(params))
        sql = '{0}({1})'.format(self.procedure_name, ap)
        return sql, params


@ICompilable.register
class Entity(Node):

    def __init__(self, entity):
        self.entity = entity

    def compile(self, compiler):
        if isinstance(self.entity, ICompilable):
            return self.entity.compile(compiler)
        if isinstance(self.entity, IField):
            return self.entity.get_sqlname(), None
        if issubclass(self.entity, IModel):
            return '"{0}"'.format(self.entity.get_meta().get_tablename()), None
        return compiler.formatter, [self.entity]

E = Entity
