from asyncorm.interfaces.query import IQueryBuilder
from asyncorm.interfaces.field import IField
from asyncorm.interfaces.model import IModel
from asyncorm.interfaces.compiler import ICompilable
from asyncorm.expressions.util import compile_value, compile_iterable


@ICompilable.register
class JoinExpressions(object):
    delimiter = ' '

    def __init__(self, values):
        self.values = values

    def compile(self, compiler):
        sql, params = compile_iterable(self.values, compiler)
        return self.delimiter.join(sql), params


class ValuesList(JoinExpressions):
    def compile(self, compiler):
        sql, params = [], []
        for values in self.values:
            _sql, _params = compile_iterable(values, compiler)
            sql.append(', '.join(_sql))
            params.extend(_params)
        return '({0})'.format('), ('.join(sql)), params


class FieldsList(JoinExpressions):
    delimiter = ', '

    def compile(self, compiler):
        sql, params = [], []
        for value in self.values:
            if isinstance(value, ICompilable):
                _sql, _params = compile_value(value, compiler)
                if _sql:
                    sql.append(_sql)
                if _params:
                    params.extend(_params)
            elif isinstance(value, IField):
                sql.append(value.get_sqlname())
            else:
                sql.append(value)
        return self.delimiter.join(sql), params


@ICompilable.register
class SetValue(object):
    def __init__(self, key, value):
        self.key = key
        self.value = value

    def compile(self, compiler):
        if isinstance(self.key, IField):
            field_name = self.key.get_sqlname(False)
        else:
            field_name = self.key
        sql, params = compile_value(self.value, compiler)
        return '{0} = {1}'.format(field_name, sql), params


@ICompilable.register
class AS(object):
    def __init__(self, field, as_field):
        self.field = field
        self.as_field = as_field

    def compile(self, compiler):
        sql, params = compile_value(self.field, compiler)
        return '{0} AS "{1}"'.format(sql, self.as_field), params


@ICompilable.register
class ASC(object):
    def __init__(self, field):
        self.field = field

    def compile(self, compiler):
        params = None
        if isinstance(self.field, IField):
            sql = self.field.get_sqlname()
        elif isinstance(self.field, ICompilable):
            sql, params = self.field.compile(compiler)
            if isinstance(self.field, IQueryBuilder):
                sql = '({0})'.format(sql)
        else:
            sql = self.field
        return '{0} {1}'.format(sql, self.__class__.__name__), params


class DESC(ASC):
    pass


@ICompilable.register
class Join(object):
    pattern = 'INNER JOIN "{0}" ON ({1})'

    def __init__(self, table, on):
        self.table = table
        self.on = on

    def get_table_name(self):
        if isinstance(self.table, str):
            return self.table
        if (isinstance(self.table, IModel) or
                issubclass(self.table, IModel)):
            return self.table.get_meta().get_tablename()

    def compile(self, compiler):
        sql, params = compile_value(self.on, compiler)
        return self.pattern.format(self.get_table_name(), sql), params


class LeftJoin(Join):
    pattern = 'LEFT OUTER JOIN "{0}" ON ({1})'


class RightJoin(Join):
    pattern = 'RIGHT OUTER JOIN "{0}" ON ({1})'
