# -*-coding:utf-8-*-
from twisted.trial import unittest
from asyncorm import models
from asyncorm.router import SimpleRouter, register_router
from asyncorm.backends.backend import BackendBase
from asyncorm.backends.compiler import SQLCompiler
from asyncorm.expressions.array import ARRAY


class TestBackend(BackendBase):
    compiler = SQLCompiler()

    def __init__(self):
        self.pool = None


class TestArray(unittest.TestCase):

    class TestModel(models.Model):
        class Meta:
            db_table = 'test_model'

        name = models.CharField(max_length=10)

    def setUp(self):
        backend = TestBackend()
        self.compiler = backend.compiler
        router = SimpleRouter(backend)
        register_router(router)

    def test_array_from_list(self):
        val = [1, 2, 3, 4]
        array = ARRAY(*val)
        sql, params = array.compile(self.compiler)
        self.assertEqual(sql, 'ARRAY[%s, %s, %s, %s]')
        self.assertEqual(params, val)

    def test_array_from_query(self):
        query = self.TestModel.manager.select('id').filter(name='test')
        array = ARRAY(query)
        sql, params = array.compile(self.compiler)
        self.assertEqual(sql, 'ARRAY[(SELECT "test_model"."id" FROM "test_model" WHERE "test_model"."name" = %s)]')
        self.assertEqual(params, ['test'])