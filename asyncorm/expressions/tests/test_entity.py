from twisted.trial import unittest
from asyncorm.interfaces.compiler import ICompilable
from asyncorm.models.model import Model
from asyncorm.backends.compiler import SQLCompiler
from asyncorm.expressions.entity import Table, Procedure


class ModelTableTest(unittest.TestCase):
    class FakeModel(Model):
        class Meta:
            db_table = 'some_table'

    def setUp(self):
        self.table_name = '"{0}"'.format(self.FakeModel._meta.db_table)
        self.table = Table(self.FakeModel)
        self.compiler = SQLCompiler()

    def test_interface(self):
        self.assertTrue(ICompilable.providedBy(self.table))

    def test_table(self):
        sql, params = self.table.compile(self.compiler)
        self.assertEqual(sql, self.table_name)
        self.assertFalse(params)


class ProcedureTableTest(unittest.TestCase):
    class FakeProcedure(Procedure):
        procedure_name = 'MyProcedure'

    def setUp(self):
        self.param1 = 1
        self.param2 = 'test'
        self.param3 = [1, 2, 3]
        self.procedure = self.FakeProcedure(self.param1, self.param2,
                                            self.param3)
        self.sql_expr = 'MyProcedure(%s, %s, %s)'
        self.compiler = SQLCompiler()

    def test_interface(self):
        self.assertTrue(ICompilable.providedBy(self.procedure))

    def test_procedure(self):
        sql, params = self.procedure.compile(self.compiler)
        self.assertEqual(sql, self.sql_expr)
        self.assertEqual(len(params), 3)
        self.assertEqual(params[0], self.param1)
        self.assertEqual(params[1], self.param2)
        self.assertEqual(params[2], self.param3)