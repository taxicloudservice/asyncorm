import asyncio
import sys
from copy import copy
from functools import wraps
from asyncorm.interfaces.field import IField
from asyncorm.interfaces.relation import IRelation
from asyncorm.interfaces.query import IQueryBuilder
from asyncorm.interfaces.compiler import ICompilable
from asyncorm.constants import EXPR_DELIMITER
from asyncorm.query import QueryContainer
from asyncorm.expressions import ASC, DESC, Node
from asyncorm.expressions import RawExpression, COUNT, AS, AND, Join, LeftJoin


@IQueryBuilder.register
class QueryManagerBase(Node):

    def __init__(self, backend, model):
        self.model = model
        self.backend = backend
        self.container = QueryContainer()
        self.container.set_table(model)
        self.fetch_result = False

    def set_backend(self, backend):
        self.backend = backend

    def _get_query_compiler(self, compiler):
        raise NotImplementedError()

    def _get_query_executor(self, backend):
        raise NotImplementedError()

    def compile(self, compiler):
        compiler = self._get_query_compiler(compiler)
        return compiler(self.container)

    def execute(self, backend=None):
        backend = backend or self.backend
        sql, params = self.compile(backend.get_compiler())
        executor = self._get_query_executor(backend)
        return executor(sql, params, self.fetch_result)

    def clone(self):
        new = self.__class__(self.backend, self.model)
        new.container = self.container.clone()
        return new

    copy = clone


class SimpleSelectableManagerBase(QueryManagerBase):

    def _get_expression_by_string(self, string_expr, value):
        pieces = string_expr.split(EXPR_DELIMITER, 1)
        expr = 'equals'
        parent = self.model
        for piece in pieces:
            attr = getattr(parent, piece)
            if isinstance(attr, IField):
                parent = attr
            else:
                expr = piece
        return getattr(parent, expr)(value)

    def block(self, *args, **kwargs):
        expr_list = list(args)
        for k, v in kwargs.items():
            expr_list.append(self._get_expression_by_string(k, v))
        return AND(expr_list)

    Q = block

    def filter(self, *args, **kwargs):
        for expr in args:
            self.container.add_where(expr)
        for k, v in kwargs.items():
            self.container.add_where(self._get_expression_by_string(k, v))
        return self

    def where(self, sql, *params):
        self.container.add_where(RawExpression(sql, params))
        return self

    def all(self):
        return self.execute()


class ReturningManagerMixin(object):

    _return_models = True

    def returning(self, *fields):
        if not self.backend.get_features().support_returning:
            raise AttributeError("Backend {0} not support returning".format(
                self.backend))
        fls = []
        self.fetch_result = True
        self._return_models = False
        for field in fields:
            if isinstance(field, str):
                field = getattr(self.model, field)
            elif isinstance(field, (IField, ICompilable)):
                pass
            else:
                raise ValueError("Model {0} has not field {1}".format(
                    self.model, field))
            fls.append(field)
        self.container.add_returning(fls)
        return self


class SelectManager(SimpleSelectableManagerBase):

    def __init__(self, backend, model, fields=None):
        super().__init__(backend, model)
        self.fetch_result = True
        self._available_models = [model]
        self._related_prefetches = {}
        self._load_to_model = not fields
        self._lazy_callbacks = []
        self._single = False
        self._set_fields(fields or self.model.get_meta().iterfields())

    def _set_fields(self, fields):
        select_fields = []
        for field in fields:
            if isinstance(field, str):
                field = self._get_field_by_string(field)
            select_fields.append(field)
        self.container.add_fields(select_fields)

    def _get_field_by_string(self, expression):
        pieces = expression.split(EXPR_DELIMITER)
        parent = self.model
        for piece in pieces:
            attr = getattr(parent, piece)
            if isinstance(attr, IRelation):
                parent = self._join_relation(attr)
            else:
                parent = attr
        if not isinstance(parent, IField):
            parent = parent.pk
        return parent

    F = _get_field_by_string

    def _get_expression_by_string(self, string_expr, value):
        pieces = string_expr.split(EXPR_DELIMITER)
        expr = 'equals'
        parent = self.model
        for piece in pieces:
            attr = getattr(parent, piece)
            if isinstance(attr, IRelation):
                if value is None or pieces[-1] == 'isnull' and value is True:
                    join_type = LeftJoin
                else:
                    join_type = Join
                parent = self._join_relation(attr, join_type)
            elif isinstance(attr, IField):
                parent = attr
            else:
                expr = piece
        return getattr(parent, expr)(value)

    def _join_relation(self, relation, join_type=None):
        related_model = relation.get_related_model()
        if related_model not in self._available_models:
            self._available_models.append(related_model)
            self.container.add_join(relation.get_join(join_type))
        return related_model

    def callback_lazy(self, callback, *args, **kwargs):
        self._lazy_callbacks.append((callback, args, kwargs))

    def join(self, model, on):
        self.container.add_join(Join(model, on))
        return self

    def limit(self, limit):
        self.container.set_limit(limit)
        return self

    def offset(self, offset):
        self.container.set_offset(offset)
        return self

    def prefetch_related(self, name):
        relation = getattr(self.model, name)
        query = relation.related_model.manager.select()
        self._related_prefetches[name] = query
        return query

    async def count(self):
        self.container.remove_fields(self.model.get_meta().fields.values())
        self._set_fields((COUNT(self.model),))
        self._load_to_model = False
        self._single = True
        result = await self.execute()
        return result[-1]

    def annotate(self, **kwargs):
        fields = []
        for k, v in kwargs.items():
            fields.append(AS(v, k))
        self.container.add_fields(fields)
        return self

    def distinct(self, *args):
        fields = []
        for field in args:
            if isinstance(field, str):
                field = self._get_field_by_string(field)
            fields.append(field)
        self.container.add_distinct(fields)
        return self

    def order_by(self, *fields):
        expressions = []
        for f in fields:
            if isinstance(f, str):
                if f.startswith('-'):
                    expr = DESC
                    f = f[1:]
                else:
                    expr = ASC
                field = self._get_field_by_string(f)
                expressions.append(expr(field))
            elif isinstance(f, IField):
                expressions.append(ASC(f))
            elif isinstance(f, ICompilable):
                expressions.append(f)
            else:
                raise ValueError("Bad attribute for ordering {0}".format(f))
        self.container.add_order_by(expressions)
        return self

    def group_by(self, *args):
        fields = []
        for field in args:
            if isinstance(field, str):
                field = self._get_field_by_string(field)
            elif isinstance(field, IField):
                pass
            else:
                raise ValueError("Bad attribute for function group by {0}".format(field))
            fields.append(field)
        self.container.add_group_by(fields)
        return self

    def single(self):
        self._single = True
        self.limit(1)
        return self

    def first(self):
        self.single()
        self.order_by(self.model.pk)
        return self.execute()

    def last(self):
        self.single()
        self.order_by(DESC(self.model.pk))
        return self.execute()

    def get(self, *args, **kwargs):
        self.single()
        self.filter(*args, **kwargs)
        return self.execute()

    def __getitem__(self, item):
        self.single()
        if item >= 1:
            self.offset(item - 1)
        return self.execute()

    def __getslice__(self, offset, limit):
        if limit != sys.maxsize:
            self.limit(limit - offset)
        if offset:
            self.offset(offset)
        return self.execute()

    def _apply_meta_ordering(self):
        if (self._load_to_model and not self.container._order_by and
                self.model.get_meta().ordering):
            self.order_by(*self.model.get_meta().ordering)

    def _get_query_compiler(self, compiler):
        return compiler.compile_select

    def _get_query_executor(self, backend):
        return backend.db_select

    def compile(self, compiler):
        self._apply_meta_ordering()
        return super().compile(compiler)

    async def execute(self, backend=None, require_related=True):
        result = await super().execute(backend)
        if self._load_to_model:
            result = [self.model.load(res) for res in result]
        if self._single:
            if not result:
                raise self.model.DoesNotExist()
            result = result[0]
        if self._lazy_callbacks:
            for callback, args, kwargs in self._lazy_callbacks:
                callback(result, *args, **kwargs)
        if self._related_prefetches:
            try:
                await self._load_related(result, require_related)
            except Exception as err:
                exc_name = err.__class__.__name__
                if require_related or exc_name != 'DoesNotExist':
                    raise
        return result

    async def _load_related(self, result, require_related):
        models = result if isinstance(result, list) else (result,)
        for model in models:
            for rel_name, query in self._related_prefetches.items():
                relation = getattr(self.model, rel_name)
                related_value = relation.extract_related_value(model)
                if related_value is None:
                    continue
                q = relation.build_filter(query.clone(), related_value)
                rel = getattr(model, rel_name)
                rel_value = await q.execute(require_related=require_related)
                rel.set_cache(rel_value)
        return result

    def clone(self):
        new = super().clone()
        new._available_models = copy(self._available_models)
        new._related_selects = self._related_prefetches.copy()
        new._load_to_model = self._load_to_model
        new._single = self._single
        return new

    def delete(self):
        q = self.model.manager.delete()
        q.container = self.container.clone()
        q._available_models = copy(self._available_models)
        return q

    def update(self, **kwargs):
        q = self.model.manager.update()
        q.container = self.container.clone()
        q.set(**kwargs)
        return q


class InsertManagerMixin(object):

    def _values_dict(self, model):
        values = []
        for field in self.model.get_meta().iterfields(True):
            value = getattr(model, field.get_attrname(), None)
            if not isinstance(value, (IField, ICompilable)):
                value = field.to_sql(value)
            values.append(value)
        return values

    def _set_pk(self, result):
        for model, pk_value in zip(self.models, result):
            model.set_pk(pk_value[0])
        return self.models

    def _get_query_compiler(self, compiler):
        return compiler.compile_insert

    def _get_query_executor(self, backend):
        return backend.db_insert


class BulkInsertManager(InsertManagerMixin, QueryManagerBase,
                        ReturningManagerMixin):

    def __init__(self, backend, model, *models):
        super().__init__(backend, model)
        self.fetch_result = True
        self.models = []
        fls = self.model.get_meta().iterfields(True)
        self.container.add_fields(f.get_sqlname(False) for f in fls)
        self.container.add_returning([self.model.pk.get_sqlname()])
        self.add(*models)

    def add(self, *models):
        values = map(self._values_dict, models)
        self.container.add_values(values)
        self.models.extend(models)
        return self

    def _returned(self, result):
        self._set_pk(result)
        return [v[1:] for v in result]

    async def execute(self, backend=None):
        result = await super().execute(backend)
        if self._return_models:
            return self._set_pk(result)
        return self._returned(result)


class SingleInsertManager(InsertManagerMixin, QueryManagerBase):

    def __init__(self, backend, model, *models):
        super().__init__(backend, model)
        self.fetch_result = True
        self.models = []
        fls = self.model.get_meta().iterfields(True)
        self.fields = [f.get_sqlname(False) for f in fls]
        self.add(*models)

    def add(self, *models):
        self.models.extend(models)
        return self

    async def execute(self, backend=None):
        backend = backend or self.backend
        compiler = self._get_query_compiler(backend.get_compiler())
        executor = self._get_query_executor(backend)
        coro_list = []
        for model in self.models:
            container = QueryContainer()
            container.set_table(model.__class__)
            container.add_fields(self.fields)
            container.add_values([self._values_dict(model)])
            sql, params = compiler(container)
            coro_list.append(executor(sql, params))
        done, pending = await asyncio.wait(coro_list)
        return self._set_pk([res.result()[0] for res in done])


class UpdateManager(SimpleSelectableManagerBase, ReturningManagerMixin):

    def __init__(self, backend, model, **values):
        super().__init__(backend, model)
        if values:
            self.set(**values)

    def set(self, **kwargs):
        values = {}
        for str_expr, value in kwargs.items():
            pieces = str_expr.split(EXPR_DELIMITER, 1)
            field = self.model.get_meta().fields[pieces[0]]
            if len(pieces) > 1:
                value = getattr(field, pieces[1])(value)
            elif isinstance(value, (IField, ICompilable)):
                pass
            else:
                value = field.to_sql(value)
            values[field.get_sqlname(False)] = value
        self.container.add_update(values)
        return self

    def _get_query_compiler(self, compiler):
        return compiler.compile_update

    def _get_query_executor(self, backend):
        return backend.db_update

    def for_update(self, nowait=False):
        self.query.for_update(nowait)
        return self


class DeleteManager(SimpleSelectableManagerBase, ReturningManagerMixin):

    def _get_query_compiler(self, compiler):
        return compiler.compile_delete

    def _get_query_executor(self, backend):
        return backend.db_delete


class QueryBuilder(object):

    def __init__(self, model, router):
        self.model = model
        self._router = router

    def select(self, *fields):
        backend = self._router.for_select(self.model)
        return SelectManager(backend, self.model, fields)

    def insert(self, *instances):
        backend = self._router.for_insert(self.model)
        if backend.get_features().support_returning:
            return BulkInsertManager(backend, self.model, *instances)
        return SingleInsertManager(backend, self.model, *instances)

    def update(self, **values):
        backend = self._router.for_update(self.model)
        return UpdateManager(backend, self.model, **values)

    def delete(self):
        backend = self._router.for_delete(self.model)
        return DeleteManager(backend, self.model)

    def transaction(self, func):
        @wraps(func)
        def wraper(*args, **kwargs):
            backend = self._router.for_update(self.model)
            return backend.db_transaction(func, *args, **kwargs)
        return wraper

    async def get_or_create(self, **kwargs):
        try:
            result = await self.select().get(**kwargs)
            created = False
        except self.model.DoesNotExist:
            result = self.model(**kwargs)
            await result.save()
            created = True
        return result, created

    def AS(self, name=None):
        from asyncorm.models import wrappers
        model = wrappers.ModelWrapper(self.model)
        return type(self)(model, self._router)

    def __getattr__(self, item):
        return getattr(self.select(), item)


class Manager(object):

    def __init__(self, router):
        self._router = router

    def __get__(self, instance, model):
        return self.create_query_builder(model)

    def create_query_builder(self, model):
        return QueryBuilder(model, self._router)
