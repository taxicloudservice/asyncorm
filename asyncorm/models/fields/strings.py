from asyncorm.models.fields.base import Field
from asyncorm.models.fields.validators import (EmailValidator, IPv4Validator,
    URLValidator)


__all__ = ['CharField', 'TextField', 'EmailField', 'URLField', 'IPAddressField']


class CharField(Field):

    def __init__(self, *args, **kwargs):
        Field.__init__(self, *args, **kwargs)
        if self.blank and not self.null and not self.has_default():
            self.default = ''

    def encoder(self, value):
        if not isinstance(value, str):
            return str(value)
        return value


class TextField(CharField):
    pass


class SpecialFieldBase(CharField):
    max_length = None
    validator = EmailValidator

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = self.max_length
        CharField.__init__(self, *args, **kwargs)

    def init_validators(self):
        CharField.init_validators(self)
        self.validators.append(self.validator)


class EmailField(SpecialFieldBase):
    max_length = 75
    validator = EmailValidator


class URLField(SpecialFieldBase):
    max_length = 15
    validator = URLValidator


class IPAddressField(SpecialFieldBase):
    max_length = 15
    validator = IPv4Validator
