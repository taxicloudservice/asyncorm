from asyncorm.expressions.array import ArrayNode
from asyncorm.expressions.operator import Is, IsNot
from asyncorm.models.fields.base import BaseField
from asyncorm.models.fields.validators import ValidatorBase, ValidationError


__all__ = ['IntegerArrayField', 'FloatArrayField', 'CharArrayField',
           'TextArrayField']


class ArrayFieldValidator(ValidatorBase):

    def validate(self, value):
        if not (isinstance(value, (list, tuple, set)) or value is None):
            raise ValidationError("Value for array field must be iterable or None")


class BaseArrayField(BaseField, ArrayNode):
    _encoder = NotImplemented

    def __init__(self, *args, **kwargs):
        super(BaseArrayField, self).__init__(*args, **kwargs)
        self.validators.append(ArrayFieldValidator())

    def encoder(self, value):
        return [self._encoder(v) for v in value]

    def isnull(self, isnull):
        if isnull:
            return Is(self, None)
        return IsNot(self, None)


class IntegerArrayField(BaseArrayField):
    _encoder = int


class FloatArrayField(BaseArrayField):
    _encoder = float


class CharArrayField(BaseArrayField):
    _encoder = str


class TextArrayField(CharArrayField):
    pass