import pytz
from datetime import datetime
from asyncorm.models.fields.base import Field


__all__ = ['DateField', 'TimeField', 'DateTimeField']


class DateTimeField(Field):
    default_pattern = '%Y-%m-%dT%H:%M:%S'

    def __init__(self, auto_now=Field, auto_now_add=None, pattern=None,
                 timezone='utc', *args, **kwargs):
        self.pattern = pattern or self.default_pattern
        self.auto_now = auto_now
        self.auto_now_add = auto_now_add
        self.timezone = timezone
        self._tz = pytz.timezone(timezone)
        Field.__init__(self, *args, **kwargs)
        if self.auto_now_add and not self.has_default():
            self.default = self._get_auto_now

    def _get_auto_now(self):
        return self._extract_intent(datetime.now(self._tz))

    def _extract_intent(self, dt):
        return dt

    def get_copy_kwargs(self):
        kwargs = Field.get_copy_kwargs(self)
        kwargs['pattern'] = self.pattern
        kwargs['auto_now'] = self.auto_now
        kwargs['auto_now_add'] = self.auto_now_add
        kwargs['timezone'] = self.timezone
        return kwargs

    def encoder(self, value):
        if isinstance(value, datetime):
            return self._extract_intent(value)
        if isinstance(value, (str, str)):
            return self._extract_intent(datetime.strptime(value, self.pattern))
        return value


class DateField(DateTimeField):
    default_pattern = '%Y-%m-%d'

    def _extract_intent(self, dt):
        return dt.date()


class TimeField(DateTimeField):
    default_pattern = '%H:%M:%S'

    def _extract_intent(self, dt):
        return dt.time()
