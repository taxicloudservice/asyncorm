import re


class ValidationError(Exception):
    pass


class ValidatorBase(object):

    def validate(self, value):
        raise NotImplementedError()

    def __call__(self, value):
        return self.validate(value)


class ValueValidator(ValidatorBase):
    validator = lambda self, a, b: None
    message = ''

    def __init__(self, limit=None):
        self.limit = limit

    def validate(self, value):
        if self.validator(value, self.limit):
            raise ValidationError(self.message.format(value, self.limit))


class MaxLengthValidator(ValueValidator):
    validator = lambda self, a, b: len(a) > b
    message = 'Length of value {0} is more {1} symbols'


class AllowedValuesValidator(ValueValidator):
    validator = lambda self, a, b: a not in b
    message = 'Wrong value {0}. Possible values {1}'


class NotNullValidator(ValueValidator):
    validator = lambda self, a, b: a is None
    message = 'Value is null'


class MaxValueValidator(ValueValidator):
    validator = lambda self, a, b: a > b
    message = 'Value {0} is greater {1}'


class MinValueValidator(ValueValidator):
    validator = lambda self, a, b: a < b
    message = 'Value {0} is less {1}'


class RegexValidator(ValidatorBase):
    message = 'Value {0} doesn`t match pattern {1}'

    def __init__(self, pattern, message=None):
        self.pattern = pattern
        self.regex = re.compile(pattern)
        if message:
            self.message = message

    def validate(self, value):
        if not bool(self.regex.search(value)):
            raise ValidationError(self.message.format(value, self.pattern))


EmailValidator = RegexValidator(r'\w+@\w+\.\w+', 'Email {0} validation error')
URLValidator = RegexValidator(r'\w{2,5}:\/\/\w+\.\w+.',
                              'URL {0} validation error')
IPv4Validator = RegexValidator(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}',
                               'IPv4 address {0} validation error')
