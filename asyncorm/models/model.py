import asyncio
from functools import partial
from collections import namedtuple
from asyncorm.log import logger
from asyncorm.interfaces.field import IField
from asyncorm.interfaces.model import IModelOptions, IModel
from asyncorm.interfaces.relation import IRelatedField, IManyToManyField, IRelation
from asyncorm.models.fields.digits import IntegerField
from asyncorm.models.fields.base import PrimaryKeyAbstractField
from asyncorm.models.manager import Manager
from asyncorm.router import default_router
from asyncorm.utils.async_utils import maybe_async


@IModelOptions.register
class ModelOptions(object):
    def __init__(self, name, meta):
        self.name = name
        self.db_table = getattr(meta, 'db_table', name)
        self.ordering = getattr(meta, 'ordering', None)
        self.abstract = getattr(meta, 'abstract', False)
        self.m2m_fields = {}
        self.foreign_keys = {}
        self.fields = {}
        self.pk_field = None

    def get_tablename(self):
        return self.db_table

    def iterfields(self, exclude_pk=False):
        if exclude_pk:
            fields = self.fields.copy()
            del fields[self.pk_field.get_attrname()]
        else:
            fields = self.fields
        return fields.values()


class Trigger(object):

    def __init__(self, trigger_name, bundle_fields):
        self.subscribers = []
        self.bundle_factory = namedtuple(trigger_name, bundle_fields)

    def connect(self, subscriber, *args, **kwargs):
        if not callable(subscriber):
            raise ValueError("Trigger subscriber must be callable")
        self.subscribers.append((subscriber, args, kwargs))

    def disconnect(self, subscriber):
        for s in self.subscribers:
            if subscriber == s[0]:
                self.subscribers.remove(s)
                break

    async def send(self, *args, **kwargs):
        bundle = self.bundle_factory(*args, **kwargs)
        for subscriber, a, kw in self.subscribers:
            try:
                await maybe_async(subscriber, bundle, *a, **kw)
            except:
                logger.exception('Got exception in trigger subscriber %r', subscriber)

    def __call__(self, *args, **kwargs):
        return self.send(*args, **kwargs)


class Registry(object):
    models = {}
    lazy_funcs = {}

    def wait_model(self, path, f):
        if path in self.models:
            f(self.models[path])
        else:
            self.lazy_funcs.setdefault(path, []).append(f)

    def add_model(self, model):
        path = '.'.join((model.__module__, model.__name__))
        self.models[path] = model
        lazy_funcs = self.lazy_funcs.pop(path, ())
        for f in lazy_funcs:
            f(model)


class ModelTriggers(object):

    def __init__(self):
        self.before_insert = Trigger('BEFORE_INSERT_TRIGGER',
                                     ('instance', 'model'))
        self.before_update = Trigger('BEFORE_UPDATE_TRIGGER',
                                     ('instance', 'model', 'changed_fields'))
        self.before_delete = Trigger('BEFORE_DELETE_TRIGGER',
                                     ('instance', 'model'))
        self.after_insert = Trigger('AFTER_INSERT_TRIGGER',
                                    ('instance', 'model'))
        self.after_update = Trigger('AFTER_UPDATE_TRIGGER',
                                    ('instance', 'model', 'changed_fields'))
        self.after_delete = Trigger('AFTER_DELETE_TRIGGER',
                                    ('instance', 'model'))


class DummyMeta(object):
    db_table = ''
    ordering = ()
    abstract = True


class DoesNotExist(Exception):
    pass


class ModelMetaclass(type):
    registry = Registry()

    def __new__(cls, name, bases, attrs):
        # Copy all fields from base abstract models
        model_attrs = {}

        def copy_fields(fields):
            return dict((n, f.copy()) for n, f in fields.items())

        for base in bases:
            base_meta = getattr(base, '_meta', None)
            if not issubclass(base, IModel):
                continue
            model_attrs.update(copy_fields(base_meta.fields))
            model_attrs.update(copy_fields(base_meta.m2m_fields))
            model_attrs.update(copy_fields(base_meta.foreign_keys))

        model_attrs.update(attrs)
        model_meta = attrs.pop('Meta', DummyMeta)
        meta = model_attrs['_meta'] = ModelOptions(name.lower(), model_meta)
        model_attrs['DoesNotExist'] = type(
            'DoesNotExist', (DoesNotExist,),
            {'__module__': '{}.{}'.format(attrs['__module__'], name)})
        model_attrs['triggers'] = ModelTriggers()
        model = type.__new__(cls, name, bases, model_attrs)
        real_fields, foreign_keys, m2m_fields = model._get_model_fields(model_attrs)
        meta.fields = real_fields
        meta.m2m_fields = m2m_fields
        meta.foreign_keys = foreign_keys
        if not meta.abstract:
            model.init_fields(real_fields)
            model.init_pk(real_fields)
            model.init_fields(foreign_keys)
            model.init_fields(m2m_fields)
            cls.registry.add_model(model)
        return model

    def _get_model_fields(cls, model_attrs):
        real_fields = {}
        foreign_keys = {}
        m2m_fields = {}
        for name, attr in model_attrs.items():
            if not isinstance(attr, IField):
                continue
            if isinstance(attr, IManyToManyField):
                fields = m2m_fields
            elif isinstance(attr, IRelatedField):
                fields = foreign_keys
            else:
                fields = real_fields
            fields[name] = attr
        return real_fields, foreign_keys, m2m_fields

    def init_fields(cls, fields):
        for name, field in fields.items():
            field.set_attrname(name)
            field.set_model(cls)

    def init_pk(cls, fields):
        pk_fields = [f for f in fields.values() if f.primary_key]
        if len(pk_fields) > 1:
            raise ValueError(('Multiple primary key fields '
                              'for model {}').format(cls))
        if pk_fields:
            cls._meta.pk_field = pk_fields[0]
        else:
            cls.id = IntegerField(unique=True, primary_key=True)
            cls._meta.pk_field = cls._meta.fields['id'] = cls.id
            cls.init_fields({'id': cls.id})
        cls.pk = PrimaryKeyAbstractField(cls._meta.pk_field)

    def __call__(cls, *args, **kwargs):
        new_obj = cls.__new__(cls, *args)
        for field in new_obj.get_meta().iterfields():
            if field.has_default():
                value = field.get_default()
            else:
                value = None
            new_obj.__dict__[field.get_attrname()] = value
        new_obj.__init__(**kwargs)
        return new_obj


@IModel.register
class Model(metaclass=ModelMetaclass):
    triggers = NotImplemented
    manager = Manager(default_router)

    def __init__(self, **kwargs):
        self.__lock = asyncio.Lock()
        self._changed_fields = set()
        for k, v in kwargs.items():
            setattr(self, k, v)

    @classmethod
    def get_meta(cls):
        return cls._meta

    @classmethod
    def load(cls, values):
        model = cls()
        for value, field in zip(values, cls.get_meta().iterfields()):
            model.__dict__[field.get_attrname()] = field.to_python(value)
        return model

    def set_pk(self, value):
        pk_field = self.get_meta().pk_field
        self.__dict__[pk_field.get_attrname()] = value

    async def save(self):
        async with self.__lock:
            model = self.__class__
            if self.pk:
                changed_fields = self._changed_fields.copy()
                self._changed_fields.clear()
                await self.triggers.before_update(self, model, changed_fields)
                await self._save_model(changed_fields)
                await self.triggers.after_update(self, model,  changed_fields)
            else:
                await self.triggers.before_insert(self, model)
                await self._save_model()
                await self.triggers.after_insert(self, model)
        return self

    async def _save_model(self, changed_fields=None):
        if self.pk and changed_fields:
            kwargs = {}
            for field_name in changed_fields:
                kwargs[field_name] = getattr(self, field_name)
            await self.manager.update(**kwargs).filter(pk=self.pk).execute()
        elif not self.pk:
            await self.manager.insert(self).execute()
        if self.get_meta().m2m_fields:
            await self._save_m2m()

    def _save_m2m(self):
        m2m_save = []
        for f in self.get_meta().m2m_fields.values():
            m2m_field = getattr(self, f.get_attrname())
            m2m_save.append(m2m_field.save())
        return asyncio.gather(*m2m_save)

    async def delete(self):
        async with self.__lock:
            if not self.pk:
                return
            model = self.__class__
            await self.triggers.before_delete(self, model)
            await self.manager.delete().filter(pk=self.pk).execute()
            self.set_pk(None)
            await self.triggers.after_delete(self, model)

    def __setattr__(self, key, value):
        super().__setattr__(key, value)
        if self.pk and key in self.get_meta().fields:
            self._changed_fields.add(key)

    def __hash__(self):
        return hash(self.pk)

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return self.pk == other.pk

