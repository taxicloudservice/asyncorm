import random
from asyncorm.interfaces.field import IField
from asyncorm.interfaces.model import IModel
from asyncorm.expressions import Node
from asyncorm.models.model import ModelOptions


@IField.register
class FieldNameWrapper(Node):

    def __init__(self, field, db_table):
        self.field = field
        self.db_table = db_table

    def __getattr__(self, item):
        return getattr(self.field, item)

    def get_sqlname(self, with_db_table=True):
        if not with_db_table:
            return '"{0}"'.format(self.db_column)
        return '"{0}"."{1}"'.format(self.db_table, self.db_column)

    def __repr__(self):
        return '<WrappedField{0} at {1:x}>'.format(self.field, id(self))


@IModel.register
class ModelWrapper(object):
    __slots__ = ('model', '_db_table', '_meta')

    class ModelOptionsWrapper(ModelOptions):

        def __init__(self, meta, db_table):
            super().__init__(meta.name, meta)
            self._meta = meta
            self.db_table = db_table
            self.pk_field = FieldNameWrapper(meta.pk_field, db_table)
            for field_name, field in meta.fields.items():
                self.fields[field_name] = FieldNameWrapper(field, db_table)

        def get_tablename(self):
            return '{0}" AS "{1}'.format(self._meta.db_table, self.db_table)

    def __init__(self, model, as_name=None):
        self.model = model
        self._db_table = as_name or 'U{0}'.format(random.randint(0, 0xff))
        self._meta = self.ModelOptionsWrapper(model._meta, self._db_table)

    def __getattr__(self, item):
        attr = getattr(self.model, item)
        if isinstance(attr, IField):
            return FieldNameWrapper(attr, self._db_table)
        return attr

    def __setattr__(self, key, value):
        if key in self.__slots__:
            super().__setattr__(key, value)
        else:
            setattr(self.model, key, value)

    def __call__(self, *args, **kwargs):
        return self.model(*args, **kwargs)

    def get_meta(self):
        return self._meta

    @property
    def manager(self):
        manager = self.model.manager
        manager.model = self
        return manager

    def __repr__(self):
        return '<WrappedModel{0} at {1:x}>'.format(self.model, id(self))
