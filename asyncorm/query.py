"""
insert('table_name', 'field1', 'field2').values(field1=2, field2=3).returning('id')
insert('field1', 'field2').into('table_name').values(field1=2, field2=3).returning('id')
insert('field1', 'field2').into('table_name').values([{'field1'=2, 'field2'=3}, {...}]).returning('id')

select('field1', 'field2').from_table('table_name').where('field1 = %s', [123])
select().from_model(Model).filter(Model.field2 == 4, Model.field3 >= 2).where('field1 = %s', [123])
select().from_procedure(MySqlFunc(1, 2, 3))[0]

update('table_name').set(field1=2, field2=3).where('field1 = %s', [123]).returning('id')

delete('table_name').where('id=%s', [123])


Model.manager.insert(model1, model2)
Model.manager.select().filter(field1=2, field2__in=[1, 2]).order_by('-field3')
Model.manager.update(
    field1__add=1,
    field2=4,
    field3=Model.manager.select('field1').filter(field2=3).limit(1)
)
Model.manager.delete().filter(field1__not_between=[1, 5])
Model.manager.select().filter(field1=2).select_related('related_name').filter(ddd=3)
"""
import sys
from copy import copy


from asyncorm.expressions.util import expression_from_string
from asyncorm.interfaces.compiler import ICompilable
from asyncorm.interfaces.model import IModel
from asyncorm.interfaces.field import IField
from asyncorm.interfaces.query import (IQueryContainer, ISelectQuery, IQueryBuilder,
    IInsertQuery, IUpdateQuery, IDeleteQuery)
from asyncorm.expressions import (Table, AND, RawExpression, JoinExpressions,
    FieldsList, SetValue, ASC, DESC, Join, LeftJoin, RightJoin, ValuesList)


@IQueryContainer.register
class QueryContainer(object):

    def __init__(self):
        self._table = None
        self._update = {}
        self._values = []
        self._values_select = None
        self._join = []
        self._where = []
        self._order_by = []
        self._group_by = set()
        self._having = []
        self._distinct = set()
        self._limit = None
        self._offset = None
        self._fields = []
        self._returning = []
        self._for_update = False
        self._for_update_nowait = False

    def add_fields(self, fields):
        self._fields.extend(fields)

    def get_fields(self):
        return FieldsList(self._fields or ('*',))

    def remove_fields(self, fields):
        map(self._fields.remove, fields)

    def add_join(self, join):
        self._join.append(join)

    def get_join(self):
        return JoinExpressions(self._join)

    def add_where(self, expr):
        self._where.append(expr)

    def get_where(self):
        if self._where:
            return AND(self._where)

    def set_insert_fields(self, fields):
        self._insert_fields.extend(fields)

    def add_values(self, values):
        self._values.extend(values)

    def get_values(self):
        if self._values:
            return ValuesList(self._values)

    def set_values_select(self, select):
        self._values_select = select

    def get_values_select(self):
        return self._values_select

    def add_update(self, kwargs):
        self._update.update(kwargs)

    def get_update(self):
        return FieldsList((SetValue(k, v) for k, v in self._update.items()))

    def add_order_by(self, fields):
        self._order_by.extend(fields)

    def get_order_by(self):
        if self._order_by:
            return FieldsList(self._order_by)

    def add_distinct(self, fields):
        for field in fields:
            if isinstance(field, IField):
                self._distinct.add(field.get_sqlname())
            else:
                self._distinct.add(field)

    def get_distinct(self):
        if self._distinct:
            return FieldsList(self._distinct)

    def set_limit(self, limit):
        self._limit = limit

    def get_limit(self):
        return self._limit

    def set_offset(self, offset):
        self._offset = offset

    def get_offset(self):
        return self._offset

    def add_group_by(self, *fields):
        self._group_by.update(*fields)

    def get_group_by(self):
        if self._group_by:
            return FieldsList(self._group_by)

    def add_having(self, expressions):
        self._having.extend(expressions)

    def get_having(self):
        if self._having:
            return AND(self._having)

    def set_table(self, table_name):
        self._table = Table(table_name)

    def get_table(self):
        return self._table

    def for_update(self, nowait=False):
        self._for_update = True
        self._for_update_nowait = nowait

    def get_for_update(self):
        return self._for_update, self._for_update_nowait

    def add_returning(self, fields):
        self._returning.extend(fields)

    def get_returning(self):
        if self._returning:
            return FieldsList(self._returning)

    def clone(self):
        new = self.__class__()
        new._table = self._table
        new._join = copy(self._join)
        new._where = copy(self._where)
        new._order_by = copy(self._order_by)
        new._group_by = self._group_by.copy()
        new._update = self._update.copy()
        new._values = copy(self._values)
        new._values_select = copy(self._values_select)
        new._having = copy(self._having)
        new._distinct = self._distinct.copy()
        new._limit = self._limit
        new._offset = self._offset
        new._fields = copy(self._fields)
        new._returning = copy(self._returning)
        new._for_update = self._for_update
        new._for_update_nowait = self._for_update_nowait
        return new

    __copy__ = copy = clone


# @implementer(IQueryBuilder)
# class QueryBuilder(object):
#
#     def __init__(self, model=None):
#         self.container = QueryContainer()
#         self.model = model
#         self._backend = None
#         self.fetch_result = False
#
#     def set_backend(self, _backend):
#         self._backend = _backend
#
#     def compile(self, compiler):
#         raise NotImplementedError()
#
#     def _get_executor(self, _backend):
#         raise NotImplementedError()
#
#     def execute(self, _backend=None):
#         _backend = _backend or self._backend
#         if not _backend:
#             raise AttributeError('Backend not exist.')
#         sql, params = self.compile(_backend.get_compiler())
#         return self._get_executor(_backend)(sql, params)
#
#     def clone(self):
#         new_builder = self.__class__()
#         new_builder.query = self.query.copy()
#         new_builder.model = self.model
#         new_builder._backend = self._backend
#         new_builder.fetch_result = self.fetch_result
#         return new_builder
#
#     __copy__ = copy = clone
#
#
# class SelectableMixin(object):
#
#     def where(self, sql, params):
#         self.container.add_where(RawExpression(sql, params))
#         return self
#
#     def filter(self, *expressions, **kwargs):
#         map(self.container.add_where, expressions)
#         for k, v in kwargs.iteritems():
#             self.container.add_where(expression_from_string(self.model, k, v))
#         return self
#
#     def _join(self, table, on_expr, join_expression):
#         expressions = []
#         for expr in on_expr:
#             if isinstance(expr, str):
#                 expr = RawExpression(expr)
#             expressions.append(expr)
#         self.container.add_join(join_expression(table, AND(expressions)))
#         return self
#
#     def join(self, table, *on):
#         return self._join(table, on, Join)
#
#     def left_join(self, table, *on):
#         return self._join(table, on, LeftJoin)
#
#     def right_join(self, table, *on):
#         return self._join(table, on, RightJoin)
#
#
# class CanReturningMixin(object):
#
#     def returning(self, *fields):
#         if self.model:
#             _fields = []
#             for field in fields:
#                 if field in self.model.get_meta().fields:
#                     _fields.append(self.model.get_meta().fields[field])
#                 else:
#                     _fields.append(field)
#             self.container.add_returning(_fields)
#         else:
#             self.container.add_returning(fields)
#         return self
#
#
# def _model_values(model, values):
#     val = {}
#     for k, v in values.iteritems():
#         field = model.get_meta().fields[k]
#         if IField.providedBy(v) or ICompilable.providedBy(v):
#             value = v
#         else:
#             value = field.to_sql(v)
#         val[field.get_sqlname(False)] = value
#     return val
#
#
# @implementer(IInsertQuery)
# class Insert(QueryBuilder, CanReturningMixin):
#
#     def __init__(self, *fields):
#         QueryBuilder.__init__(self)
#         self.fields = set(fields)
#
#     def into(self, table):
#         self.container.set_table(table)
#         if IModel.implementedBy(table):
#             self.model = table
#             model_fields = self.model.get_meta().fields.itervalues()
#             fls = [f.get_sqlname(False) for f in model_fields]
#             self.container.add_fields(fls)
#         elif self.fields:
#             self.container.add_fields(self.fields)
#         else:
#             raise ValueError("Fields for insert into table {0} not set".format(
#                 table))
#         return self
#
#     def __get_model_values_list(self, kw):
#         return _model_values(self.model, kw)
#
#     def __get_values_list(self, kw):
#         values = []
#         for field_name in self.fields:
#             values.append(kw.get(field_name))
#         return values
#
#     def values(self, *args, **kwargs):
#         values = []
#         if self.model:
#             get_values_list = self.__get_model_values_list
#         else:
#             get_values_list = self.__get_values_list
#         if kwargs:
#             values.append(get_values_list(kwargs))
#         for kw in args:
#             values.append(get_values_list(kw))
#         self.container.add_values(values)
#         return self
#
#     def from_select(self, select):
#         self.container.set_values_select(select)
#         return self
#
#     def _get_executor(self, _backend):
#         return _backend.db_insert
#
#     def compile(self, compiler):
#         return compiler.compile_insert(self.container)
#
#
# @implementer(ISelectQuery)
# class Select(QueryBuilder, SelectableMixin):
#
#     def __init__(self, *fields):
#         QueryBuilder.__init__(self)
#         self.fetch_result = True
#         if fields:
#             self.container.add_fields(fields)
#
#     def from_table(self, table_name):
#         self.container.set_table(table_name)
#         return self
#
#     def from_model(self, model):
#         self.model = model
#         if not self.container._fields:
#             model_fields = model.get_meta().fields.itervalues()
#             self.container.add_fields([f.get_sqlname() for f in model_fields])
#         self.container.set_table(model)
#         if model.get_meta().ordering:
#             self.order_by(*model.get_meta().ordering)
#         return self
#
#     def from_procedure(self, procedure):
#         self.container.set_table(procedure)
#         return self
#
#     def distinct(self, *fields):
#         self.container.add_distinct(fields)
#         return self
#
#     def having(self, *expressions):
#         self.container.add_having(expressions)
#         return self
#
#     def limit(self, limit):
#         self.container.set_limit(limit)
#         return self
#
#     def offset(self, offset):
#         self.container.set_offset(offset)
#         return self
#
#     def order_by(self, *fields):
#         order_fields = []
#         for field in fields:
#             if isinstance(field, str):
#                 if field.startswith('-'):
#                     field = field[1:]
#                     order_expr = DESC
#                 else:
#                     order_expr = ASC
#                 if self.model and field in self.model.get_meta().fields:
#                     field = self.model.get_meta().fields[field]
#                 order_fields.append(order_expr(field))
#             else:
#                 order_fields.append(field)
#         self.container.add_order_by(order_fields)
#         return self
#
#     def for_update(self, nowait=False):
#         self.container.for_update(nowait)
#         return self
#
#     def _get_executor(self, _backend):
#         return _backend.db_select
#
#     def compile(self, compiler):
#         return compiler.compile_select(self.container)
#
#     def __getitem__(self, item):
#         self.limit(1)
#         if item > 1:
#             self.offset(item - 1)
#         return self
#
#     def __getslice__(self, offset, limit):
#         if limit != sys.maxint:
#             self.limit(limit - offset)
#         if offset:
#             self.offset(offset)
#         return self
#
#
# @implementer(IUpdateQuery)
# class Update(QueryBuilder, SelectableMixin, CanReturningMixin):
#
#     def __init__(self, table):
#         model = table if IModel.implementedBy(table) else None
#         QueryBuilder.__init__(self, model)
#         self.container.set_table(table)
#
#     def set(self, **kwargs):
#         if self.model:
#             kwargs = _model_values(self.model, kwargs)
#         self.container.add_update(kwargs)
#         return self
#
#     def _get_executor(self, _backend):
#         return _backend.db_update
#
#     def compile(self, compiler):
#         return compiler.compile_update(self.container)
#
#
# @implementer(IDeleteQuery)
# class Delete(QueryBuilder, SelectableMixin, CanReturningMixin):
#
#     def __init__(self, table):
#         model = table if IModel.implementedBy(table) else None
#         QueryBuilder.__init__(self, model)
#         self.container.set_table(table)
#
#     def _get_executor(self, _backend):
#         return _backend.db_delete
#
#     def compile(self, compiler):
#         return compiler.compile_delete(self.container)