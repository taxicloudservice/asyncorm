import asyncio


def maybe_async(func, *args, **kwargs):
    try:
        res = func(*args, **kwargs)
    except Exception as err:
        return errback(err)
    if asyncio.iscoroutine(res):
        return res
    if isinstance(res, asyncio.Future):
        return res
    return success(res)


def errback(exception):
    future = asyncio.Future()
    future.set_exception(exception)
    return future


def success(result):
    future = asyncio.Future()
    future.set_result(result)
    return future
