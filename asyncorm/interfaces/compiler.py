import abc


class ICompilable(abc.ABC):
    """
    Interface for object who can compiled to sql query
    """

    @abc.abstractmethod
    def compile(self, compiler):
        """
        Compile this object to sql query

        :param compiler: implementer if `ICompiler`
        :return: sql, params
        """


class ICompiler(abc.ABC):
    """
    SQL compiler.
    """

    @abc.abstractmethod
    def compile_insert(self, container):
        """
        Compile query to SQL INSERT

        :param container:  implementer of `ICompiled`, `IQueryContainer`
        :return: sql, args
        """

    @abc.abstractmethod
    def compile_select(self, container):
        """
        Compile query to SQL SELECT

        :param container:  implementer of `ICompiled`, `IQueryContainer`
        :return: sql, args
        """

    @abc.abstractmethod
    def compile_update(self, container):
        """
        Compile query to SQL UPDATE

        :param container:  implementer of `ICompiled`, `IQueryContainer`
        :return: sql, args
        """

    @abc.abstractmethod
    def compile_delete(self, container):
        """
        Compile query to SQL DELETE

        :param container:  implementer of `ICompiled`, `IQueryContainer`
        :return: sql, args
        """

    @abc.abstractmethod
    def get_expression_pattern(self, expr):
        """
        Get db expression pattern

        :param expr: python expressions name
        :return: sql expression pattern
        :rtype: str
        """

    @abc.abstractmethod
    def get_formatter(self):
        """
        Get specific db driver formatter
        :return: formatter f ex (? or %s)
        """
