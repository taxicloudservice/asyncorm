import abc
from asyncorm.interfaces.transaction import ITransaction


class IBackend(ITransaction):
    """
    Database backed.

    Provide executing queries
    """
    formatter = NotImplemented
    support_returning = NotImplemented
    support_for_update = NotImplemented


    @abc.abstractmethod
    def start(self):
        """
        Start this _backend. Will init the network connection or other

        :return: defer
        """

    @abc.abstractmethod
    def stop(self):
        """
        Stop this _backend

        :return: defer
        """

