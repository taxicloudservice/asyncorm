import abc
from asyncorm.interfaces.expression import IExpression


class IField(IExpression):
    abstract = NotImplemented

    @abc.abstractmethod
    def to_sql(self, value):
        """
        Convert python data to sql

        :param value: python data
        :return: sql data
        """

    @abc.abstractmethod
    def to_python(self, value):
        """
        Convert sql data to python object

        :param value: sql data
        :return: python object
        """
