import abc


class IRouter(abc.ABC):
    """
    DB router

    contains IBackend implementers and return his for specific actions
    """

    @abc.abstractmethod
    def for_delete(self, model):
        """
        get _backend for db delete

        :param model: implementer `IModel`
        :return: implementer `IBackend`
        """

    @abc.abstractmethod
    def for_update(self, model):
        """
        get _backend for db update

        :param model: implementer `IModel`
        :return: implementer `IBackend`
        """

    @abc.abstractmethod
    def for_select(self, model):
        """
        get _backend for db select

        :param model: implementer `IModel`
        :return: implementer `IBackend`
        """

    @abc.abstractmethod
    def for_insert(self, model):
        """
        get _backend for db insert

        :param model: implementer `IModel`
        :return: implementer `IBackend`
        """
