import abc
from asyncorm.interfaces.compiler import ICompilable


class IQueryContainer(abc.ABC):
    """
    Container for query
    """


class IQueryBuilder(ICompilable):
    """
    Base interface for query builder
    """

    @abc.abstractmethod
    def set_backend(self, backend):
        """
        Set _backend for this builder

        :param backend: `IBackend` implementer
        """

    @abc.abstractmethod
    def execute(self, backend):
        """
        Execute this query

        :return: defer.Deferred
        """

    @abc.abstractmethod
    def clone(self):
        """
        Make copy of this object
        """


class IInsertQuery(IQueryBuilder):
    """
    SQL select query interface
    """


class ISelectQuery(IQueryBuilder):
    """
    SQL select query interface
    """


class IUpdateQuery(IQueryBuilder):
    """
    SQL select query interface
    """


class IDeleteQuery(IQueryBuilder):
    """
    SQL select query interface
    """