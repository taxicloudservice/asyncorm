from asyncorm.interfaces.router import IRouter


@IRouter.register
class AbstractRouter(object):

    def for_delete(self, model):
        raise NotImplementedError()

    def for_update(self, model):
        raise NotImplementedError()

    def for_select(self, model):
        raise NotImplementedError()

    def for_insert(self, model):
        raise NotImplementedError()


class SimpleRouter(AbstractRouter):
    def __init__(self, backend):
        self._backend = backend

    def for_delete(self, model):
        return self._backend

    def for_update(self, model):
        return self._backend

    def for_select(self, model):
        return self._backend

    def for_insert(self, model):
        return self._backend

    def set_backend(self, backend):
        self._backend = backend


class MasterSlaveRouter(AbstractRouter):
    def __init__(self, master, *slaves):
        self.master = master
        self.slaves = slaves
        self.next_slave = 0

    def for_insert(self, model):
        return self.master

    def for_update(self, model):
        return self.master

    def for_delete(self, model):
        return self.master

    def for_select(self, model):
        if len(self.slaves) >= self.next_slave:
            self.next_slave = 0
        s = self.slaves[self.next_slave]
        self.next_slave += 1
        return s


default_router = SimpleRouter(None)

