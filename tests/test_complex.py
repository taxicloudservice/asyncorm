import unittest
import aio.testing
from asyncorm import models, router
from .backend import FakeBackend


class TestModel(models.Model):

    class Meta:
        db_table = 'test_table'

    field1 = models.IntegerField()
    field2 = models.CharField(max_length=30)


class TestModel2(models.Model):

    class Meta:
        db_table = 'test_table'

    field1 = models.CharField(max_length=30)
    model1 = models.ForeignKey(TestModel, related_name='model2')


class SelectTest(unittest.TestCase):

    def setUp(self):
        self.backend = FakeBackend()
        router.default_router.set_backend(self.backend)

    def tearDown(self):
        router.default_router.set_backend(None)

    @aio.testing.run_until_complete
    async def test_select(self):
        self.backend.add_select_result((1, 'test_test', 100))
        q = TestModel.manager.select().filter(field1__gte=100)
        q.filter(model2__field1='test')
        q.prefetch_related('model2')
        m = await q.execute()
        print(self.backend._db_select_queries)
    #
    # @aio.testing.run_until_complete
    # async def test_insert(self):
    #     self.backend.add_insert_result((1,))
    #     m = await TestModel(field1=100, field2='test').save()
    #     print(m.pk)
    #     print(self.backend.get_insert_queries())

    # @aio.testing.run_until_complete
    # async def test_update(self):
    #     m = TestModel(field1=100, field2='test')
    #     m.set_pk(1)
    #     m.field1 = 2
    #     await m.save()
    #     print(self.backend.get_update_queries())
    #
    # @aio.testing.run_until_complete
    # async def test_delete(self):
    #     m = TestModel(field1=100, field2='test')
    #     m.set_pk(1)
    #     await m.delete()
    #     print(self.backend.get_delete_queries())

    @aio.testing.run_until_complete
    async def test_load_related(self):
        m = TestModel(field1=100, field2='test')
        m.set_pk(1)
        # self.backend.add_select_result((1, 1))
        # model2 = await m.model2.manager.execute()
        print(self.backend.get_select_queries())


if __name__ == '__main__':
    unittest.main()
