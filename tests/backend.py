from asyncorm.backends.compiler import SQLCompiler
from asyncorm.backends.backend import BackendBase


class FakeBackend(BackendBase):
    compiler = SQLCompiler()

    def __init__(self, *args, **kwargs):
        self._db_insert_queries = []
        self._db_insert_results = []
        self._db_select_queries = []
        self._db_select_results = []
        self._db_update_queries = []
        self._db_update_results = []
        self._db_delete_queries = []
        self._db_delete_results = []

    async def _run_trans(self, executor_name, sql, params, *args, **kwargs):
        db_queries = getattr(self, '_{}_queries'.format(executor_name))
        db_results = getattr(self, '_{}_results'.format(executor_name))
        db_queries.append((sql, params))
        return db_results

    def add_insert_result(self, result):
        self._db_insert_results.append(result)

    def get_insert_queries(self):
        res = self._db_insert_queries[:]
        self._db_insert_queries = []
        return res

    def add_select_result(self, result):
        self._db_select_results.append(result)

    def get_select_queries(self):
        return self._db_select_queries

    def add_update_result(self, result):
        self._db_update_results.append(result)

    def get_update_queries(self):
        return self._db_update_queries

    def add_delete_result(self, result):
        self._db_delete_results.append(result)

    def get_delete_queries(self):
        return self._db_delete_queries
